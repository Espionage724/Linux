# Update TrinityCore
cd '/home/espionage724/trinitycore'
sudo -u espionage724 git pull origin 3.3.5
sync

# Setup Build Folder
sudo -u espionage724 rm -r '/home/espionage724/build'
sudo -u espionage724 mkdir '/home/espionage724/build'
cd '/home/espionage724/build'
sync

# Prepare Build
sudo -u espionage724 CC='distcc gcc' CXX='distcc g++' cmake '/home/espionage724/trinitycore' -DCMAKE_INSTALL_PREFIX='/home/espionage724/run' -DTOOLS=1 -DWITH_WARNINGS=0 -DWITH_COREDEBUG=0 -DUSE_COREPCH=0 -DUSE_SCRIPTPCH=0 -DCMAKE_CXX_FLAGS='-Ofast -pipe -march=amdfam10 -mmmx -m3dnow -msse -msse2 -msse3 -msse4a -mcx16 -msahf -mpopcnt -mabm -mlzcnt -mprfchw -mfxsr --param l1-cache-size=64 --param l1-cache-line-size=64 --param l2-cache-size=512 -mtune=amdfam10' -DCMAKE_C_FLAGS='-Ofast -pipe -march=amdfam10 -mmmx -m3dnow -msse -msse2 -msse3 -msse4a -mcx16 -msahf -mpopcnt -mabm -mlzcnt -mprfchw -mfxsr --param l1-cache-size=64 --param l1-cache-line-size=64 --param l2-cache-size=512 -mtune=amdfam10'
sync

# Compile TrinityCore
sudo -u espionage724 DISTCC_HOSTS='192.168.1.150/4 localhost/2' make -j6
sync

# Shutdown Servers
sudo systemctl stop auth world

# Install TrinityCore
sudo -u espionage724 make install
sync

# Restart Servers
sudo systemctl start auth world

# Finish up
cd '/home/espionage724/'