%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- OpenITG Beta 2
- vsftpd
- Minimal install for Hatebeat
- Arch Linux

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Main
%%%%%%%%%%%%%%%%%%%%

sudo pacman -S --needed vsftpd libmad libxtst glu libusb-compat libxrandr

%%%%%%%%%%%%%%%%%%%%
%%%%% libpng12 (AUR)
%%%%%%%%%%%%%%%%%%%%

wget 'https://aur.archlinux.org/cgit/aur.git/snapshot/libpng12.tar.gz' -O ~/'libpng12.tar.gz' && tar -xzvf ~/'libpng12.tar.gz' -C ~ && cd ~/'libpng12' && makepkg -sri --noconfirm && cd ~ && rm -R ~/'libpng12.tar.gz' ~/'libpng12' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% libjpeg6-turbo (AUR)
%%%%%%%%%%%%%%%%%%%%

wget 'https://aur.archlinux.org/cgit/aur.git/snapshot/libjpeg6-turbo.tar.gz' -O ~/'libjpeg6-turbo.tar.gz' && tar -xzvf ~/'libjpeg6-turbo.tar.gz' -C ~ && cd ~/'libjpeg6-turbo' && makepkg -sri --noconfirm && cd ~ && rm -R ~/'libjpeg6-turbo.tar.gz' ~/'libjpeg6-turbo' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Create User
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo useradd -r -U 'openitg' -d '/home/openitg' -m && sudo passwd 'openitg'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% FTP Server
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Firewall
%%%%%%%%%%%%%%%%%%%%

sudo ufw allow from '192.168.1.0/24' to any port '21'

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo systemctl enable 'vsftpd'

%%%%%%%%%%%%%%%%%%%%
%%%%% Configuration
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/vsftpd.conf' && sudo systemctl restart 'vsftpd'

-------------------------
anonymous_enable=NO
local_enable=YES
write_enable=YES
chroot_local_user=YES

allow_writeable_chroot=YES

pasv_enable=NO
seccomp_sandbox=NO
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Installation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Note
%%%%%%%%%%%%%%%%%%%%

- Upload entire 'StepMania' folder to '/home/openitg'

%%%%%%%%%%%%%%%%%%%%
%%%%% Switch User
%%%%%%%%%%%%%%%%%%%%

sudo su 'openitg' -s '/bin/bash'

%%%%%%%%%%%%%%%%%%%%
%%%%% OpenITG
%%%%%%%%%%%%%%%%%%%%

cd ~ && unzip ~/'StepMania/OpenITG Files/OpenITG Beta 2 - Linux.zip' -d ~ && mv ~/'StepMania/OpenITG/RandomMovies' ~/'StepMania/OpenITG/Songs' ~/'OpenITG' && mv ~/'StepMania/OpenITG/Courses/pendulum' ~/'OpenITG/Courses' && mv ~/'StepMania/OpenITG/Themes/Simply Love' ~/'OpenITG/Themes' && chmod +x ~/'OpenITG/openitg-sse2-beta-2' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Finish Up
%%%%%%%%%%%%%%%%%%%%

rm -R ~/'StepMania' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Switch User
%%%%%%%%%%%%%%%%%%%%

sudo su 'openitg' -s '/bin/bash'

%%%%%%%%%%%%%%%%%%%%
%%%%% General
%%%%%%%%%%%%%%%%%%%%

mkdir -p ~/'sdb1' ~/'Share' && nano ~/'OpenITG/Data/Static.ini'

-------------------------
[Options]
Vsync=0
VideoRenderers=opengl
SoundDrivers=alsa-sw
SignProfileData=0
SmoothLines=0
SoloSingle=1
ShowLoadingWindow=0
ShowNativeLanguage=0
MemoryCards=1
MemoryCardOsMountPointP1=/home/openitg/sdb1
MachineName=Hatebeat
GlobalOffsetSeconds=-0.065612
FastLoad=1
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% USB Memory Card Support
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/fstab'

-------------------------
# OpenITG P1 USB Memory Card
/dev/sdb1 /home/openitg/sdb1 vfat rw,user,noauto,sync 0 0
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Launcher
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Switch User
%%%%%%%%%%%%%%%%%%%%

sudo su 'openitg' -s '/bin/bash'

%%%%%%%%%%%%%%%%%%%%
%%%%% Launcher
%%%%%%%%%%%%%%%%%%%%

nano ~/'.xinitrc'

-------------------------
exec ~/'OpenITG/openitg-sse2-beta-2'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Execute
%%%%%%%%%%%%%%%%%%%%

startx

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Autostart
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Systemd
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/getty@tty1.service.d/override.conf'

-------------------------
[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --autologin openitg --noclear %I $TERM
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Switch User
%%%%%%%%%%%%%%%%%%%%

sudo su 'openitg' -s '/bin/bash'

%%%%%%%%%%%%%%%%%%%%
%%%%% Autostart xinitrc
%%%%%%%%%%%%%%%%%%%%

nano ~/'.bash_profile'

- Add
-------------------------
[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################