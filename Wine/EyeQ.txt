%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Creates isolated 32-bit prefix for EyeQ

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Installation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Install
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX='/home/espionage724/Wine Prefixes/EyeQ' WINEARCH=win32 winecfg

- Add EyeQ folder as disc image to drive E:

WINEPREFIX='/home/espionage724/Wine Prefixes/EyeQ' wine 'E:\Setup.exe'

- Key: EYEQ33PER7W

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove Autostarts
%%%%%%%%%%%%%%%%%%%%

rm '/home/espionage724/Wine Prefixes/EyeQ/drive_c/users/Public/Start Menu/Programs/StartUp/MiniEYE-MiniREAD Launch.lnk'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Launchers
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove Old
%%%%%%%%%%%%%%%%%%%%

rm -R '/home/espionage724/.local/share/applications/wine/Programs/eyeQ' && mkdir -p '/home/espionage724/.local/share/applications/wine/Programs/eyeQ'

%%%%%%%%%%%%%%%%%%%%
%%%%% MiniEYE Excercises
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/.local/share/applications/wine/Programs/eyeQ/MiniEYE Exercises.desktop'

-------------------------
[Desktop Entry]
Name=MiniEYE Exercises
Categories=Education;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/espionage724/Wine Prefixes/EyeQ' wine '/home/espionage724/Wine Prefixes/EyeQ/drive_c/Program Files/Infinite Mind LC/eyeQ/MiniEYE.exe'
Type=Application
StartupNotify=true
Path=/home/espionage724/Wine Prefixes/EyeQ/drive_c/Program Files/Infinite Mind LC/eyeQ
Icon=0383_MiniEYE.0
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% EyeQ
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/.local/share/applications/wine/Programs/eyeQ/eyeQ.desktop'

-------------------------
[Desktop Entry]
Name=eyeQ
Categories=Education;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/espionage724/Wine Prefixes/EyeQ' wine '/home/espionage724/Wine Prefixes/EyeQ/drive_c/Program Files/Infinite Mind LC/eyeQ/eyeQ.exe'
Type=Application
StartupNotify=true
Path=/home/espionage724/Wine Prefixes/EyeQ/drive_c/Program Files/Infinite Mind LC/eyeQ
Icon=8D24_eyeQ.0
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% MiniREAD Excercises
%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/.local/share/applications/wine/Programs/eyeQ/MiniREAD Exercises.desktop'

-------------------------
[Desktop Entry]
Name=MiniREAD Exercises
Categories=Education;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/espionage724/Wine Prefixes/EyeQ' wine '/home/espionage724/Wine Prefixes/EyeQ/drive_c/Program Files/Infinite Mind LC/eyeQ/MiniREAD.exe'
Type=Application
StartupNotify=true
Path=/home/espionage724/Wine Prefixes/EyeQ/drive_c/Program Files/Infinite Mind LC/eyeQ
Icon=9E88_MiniREAD.0
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################