%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Creates isolated 32-bit prefix for Zuma's Revenge!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Installation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Zuma's Revenge!
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX='/home/beowulfsdr/Wine Prefixes/Zuma' WINEARCH=win32 wine '/home/beowulfsdr/Disc Images/ZMRPC-EN/PopCDRun.exe'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Launcher
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

rm -R '/home/beowulfsdr/.local/share/applications/wine/Programs/PopCap Games' && mkdir -p "/home/beowulfsdr/.local/share/applications/wine/Programs/Zuma's Revenge"

%%%%%%%%%%%%%%%%%%%%
%%%%% Zuma's Revenge!
%%%%%%%%%%%%%%%%%%%%

nano "/home/beowulfsdr/.local/share/applications/wine/Programs/Zuma's Revenge/Zuma's Revenge.desktop"

-------------------------
[Desktop Entry]
Name=Zuma's Revenge!
Categories=Game;
Exec=env WINEDEBUG=-all WINEPREFIX='/home/beowulfsdr/Wine Prefixes/Zuma' wine '/home/beowulfsdr/Wine Prefixes/Zuma/drive_c/Program Files/PopCap Games/Zuma'\''s Revenge/ZumasRevenge.exe'
Type=Application
StartupNotify=true
Path=/home/beowulfsdr/Wine Prefixes/Zuma/drive_c/Program Files/PopCap Games/Zuma's Revenge
Icon=7B36_ZumasRevenge.0
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################