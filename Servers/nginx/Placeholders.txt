%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Placeholder site info for Realm of Espionage
- openSUSE Tumbleweed

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Main
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

sudo mkdir -p '/srv/www/main' && sudo chown nginx:nginx '/srv/www/main'

%%%%%%%%%%%%%%%%%%%%
%%%%% Site Content
%%%%%%%%%%%%%%%%%%%%

sudo -u 'nginx' -e '/srv/www/main/index.html'

-------------------------
This is the home page of Realm of Espionage.
<br>
<br>
Some quick-reference links (links open in new window):
<br>
<br>
<a href = 'https://realmofespionage.xyz' target = 'blank'>Realm of Espionage</a> - Main page (you are here)
<br>
<a href = 'https://social.realmofespionage.xyz' target = 'blank'>RoE | Social</a> - GNU social instance (social network)
<br>
<a href = 'https://media.realmofespionage.xyz' target = 'blank'>RoE | Media</a> - GNU MediaGoblin instance (media sharing)
<br>
<a href = 'https://blog.realmofespionage.xyz' target = 'blank'>RoE | Blog</a> - Blog
<br>
<a href = 'https://wow.realmofespionage.xyz' target = 'blank'>RoE (WotLK)</a> - World of Warcraft WotLK private server sign-up page
<br>
<br>
(this page will look nicer and contain more information in the future)
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% nginx Server Block
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/nginx/conf.d/main.conf' && sudo systemctl reload 'nginx'

-------------------------
server {
    listen 443 ssl spdy deferred;
    server_name realmofespionage.xyz;
    server_name_in_redirect off;
    root /srv/www/main;
    index index.html;

    #access_log /var/log/nginx/main-access.log;
    #error_log /var/log/nginx/main-error.log info;
}
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Blog
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

sudo mkdir -p '/srv/www/blog' && sudo chown nginx:nginx '/srv/www/blog'

%%%%%%%%%%%%%%%%%%%%
%%%%% Site Content
%%%%%%%%%%%%%%%%%%%%

sudo -u 'nginx' -e '/srv/www/blog/index.html'

-------------------------
This is my personal blog on Realm of Espionage.
<br>
<br>
Some quick-reference links (links open in new window):
<br>
<br>
<a href = 'https://realmofespionage.xyz' target = 'blank'>Realm of Espionage</a> - Main page
<br>
<a href = 'https://social.realmofespionage.xyz' target = 'blank'>RoE | Social</a> - GNU social instance (social network)
<br>
<a href = 'https://media.realmofespionage.xyz' target = 'blank'>RoE | Media</a> - GNU MediaGoblin instance (media sharing)
<br>
<a href = 'https://blog.realmofespionage.xyz' target = 'blank'>RoE | Blog</a> - Blog (you are here)
<br>
<a href = 'https://wow.realmofespionage.xyz' target = 'blank'>RoE (WotLK)</a> - World of Warcraft WotLK private server sign-up page
<br>
<br>
(this page will look nicer and contain actual content in the future)
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% nginx Server Block
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/nginx/conf.d/blog.conf' && sudo systemctl reload 'nginx'

-------------------------
server {
    listen 443 ssl spdy;
    server_name blog.realmofespionage.xyz;
    server_name_in_redirect off;
    root /srv/www/blog;
    index index.html;

    #access_log /var/log/nginx/blog-access.log;
    #error_log /var/log/nginx/blog-error.log info;
}
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% RoE (WotLK)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

sudo mkdir -p '/srv/www/wow' && sudo chown nginx:nginx '/srv/www/wow'

%%%%%%%%%%%%%%%%%%%%
%%%%% Site Content
%%%%%%%%%%%%%%%%%%%%

sudo -u 'nginx' -e '/srv/www/wow/index.html'

-------------------------
This will be the sign-up page for RoE (WotLK).
<br>
<br>
Some quick-reference links (links open in new window):
<br>
<br>
<a href = 'https://realmofespionage.xyz' target = 'blank'>Realm of Espionage</a> - Main page
<br>
<a href = 'https://social.realmofespionage.xyz' target = 'blank'>RoE | Social</a> - GNU social instance (social network)
<br>
<a href = 'https://media.realmofespionage.xyz' target = 'blank'>RoE | Media</a> - GNU MediaGoblin instance (media sharing)
<br>
<a href = 'https://blog.realmofespionage.xyz' target = 'blank'>RoE | Blog</a> - Blog
<br>
<a href = 'https://wow.realmofespionage.xyz' target = 'blank'>RoE (WotLK)</a> - World of Warcraft WotLK private server sign-up page (you are here)
<br>
<br>
(this page will look nicer and contain more information in the future)
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% nginx Server Block
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/nginx/conf.d/wow.conf' && sudo systemctl reload 'nginx'

-------------------------
server {
    listen 443 ssl spdy;
    server_name wow.realmofespionage.xyz;
    server_name_in_redirect off;
    root /srv/www/wow;
    index index.html;

    #access_log /var/log/nginx/wow-access.log;
    #error_log /var/log/nginx/wow-error.log info;
}
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################