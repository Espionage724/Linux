%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- GNU social
- Realm of Espionage
- Fedora Server 23

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% TODOS
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Figure out what to do about huge file/thumbs folders since they're not being automatically cleaned; maybe just delete them and hope for the best :p

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo dnf install 'git-core'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Download Source
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cd '/var/www' && sudo git clone -b 'nightly' 'https://git.gnu.io/gnu/gnu-social.git' '/var/www/social' && sudo chcon -R -t 'httpd_sys_rw_content_t' '/var/www/social' && sudo chown -R 'nginx':'nginx' '/var/www/social' && sudo chgrp -R 'nginx' '/var/lib/php/session' '/var/lib/php/wsdlcache' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Database
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mysql -u 'root' -p

CREATE DATABASE gnusocial;

GRANT ALL PRIVILEGES ON gnusocial.* to 'gnusocial'@'localhost' IDENTIFIED BY 'x';

FLUSH PRIVILEGES;
EXIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% nginx Server Block
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/nginx/conf.d/social-site.conf' && sudo systemctl reload 'nginx'

-------------------------
server {
    listen 443 ssl spdy;
    server_name social.realmofespionage.xyz;
    server_name_in_redirect off;
    root /var/www/social;
    client_max_body_size 10M;
    index index.php;

    include /etc/nginx/default.d/php.conf;

    #access_log /var/log/nginx/social-access.log;
    #error_log /var/log/nginx/social-error.log info;

    location / {
        try_files $uri $uri/ @gnusocial;
    }

    location @gnusocial {
        rewrite ^(.*)$ /index.php?p=$1 last;
    }
}
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% SELinux
%%%%%%%%%%%%%%%%%%%%

sudo setsebool -P 'httpd_can_sendmail=1'

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Setup
%%%%%%%%%%%%%%%%%%%%

- https://social.realmofespionage.xyz

%%%%%%%%%%%%%%%%%%%%
%%%%% Post-Setup
%%%%%%%%%%%%%%%%%%%%

sudo -u 'nginx' -e '/var/www/social/config.php' && sudo -u 'nginx' php '/var/www/social/scripts/checkschema.php' && sync

-------------------------
$config['db']['schemacheck'] = 'script';

$config['throttle']['enabled'] = true;
$config['throttle']['count'] = '25';
$config['throttle']['timespan'] = '3600';
$config['performance']['high'] = true;

$config['queue']['daemon'] = true;
$config['queue']['enabled'] = true;
$config['queue']['subsystem'] = 'db';

$config['profile']['delete'] = true;
$config['thumbnail']['maxsize'] = 3000;

$config['mail']['domain'] = 'realmofespionage.xyz';
$config['mail']['notifyfrom'] = '"RoE | Social" <social@realmofespionage.xyz>';
$config['mail']['backend'] = 'smtp';
$config['mail']['debug'] = true;

$config['mail']['params'] = array(
'host' => 'smtp.sendgrid.net',
'port' => 587,
'auth' => true,
'username' => 'Espionage724',
'password' => 'x'
);

addPlugin('AuthCrypt');
GNUsocial::delPlugin('OpportunisticQM');
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Services
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service Names
%%%%%%%%%%%%%%%%%%%%

social-d: Queuedaemon
social-up: GNU social updater/maintenance
social-re: GNU social Queuedaemon restarter
social-b: GNU social database backup

%%%%%%%%%%%%%%%%%%%%
%%%%% Times
%%%%%%%%%%%%%%%%%%%%

social-d: On-boot/presistent
social-up: daily @ 2:00 AM EST
social-re: daily @ 2:30 AM EST
social-b: daily @2:45 AM EST

%%%%%%%%%%%%%%%%%%%%
%%%%% queuedaemon (Service)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/social-d.service'

-------------------------
[Unit]
Description=GNU social queuedaemon
After=network.target

[Service]
Type=forking
User=nginx
Group=nginx
WorkingDirectory=/var/www/social
ExecStart='/var/www/social/scripts/startdaemons.sh'
ExecStop='/var/www/social/scripts/stopdaemons.sh'
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Updater (Service)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/social-up.service'

-------------------------
[Service]
User=nginx
Group=nginx
Type=oneshot
WorkingDirectory=/var/www/social
ExecStart='/usr/bin/git' -C '/var/www/social' pull origin 'nightly'
ExecStart='/usr/bin/php' '/var/www/social/scripts/upgrade.php'
ExecStart='/usr/bin/php' '/var/www/social/scripts/clean_file_table.php' -y
ExecStart='/usr/bin/php' '/var/www/social/scripts/clean_profiles.php' -y
ExecStart='/usr/bin/php' '/var/www/social/scripts/clean_thumbnails.php' -y
ExecStart='/usr/bin/php' '/var/www/social/scripts/delete_orphan_files.php' -y
ExecStart='/usr/bin/php' '/var/www/social/scripts/fixup_deletions.php'
ExecStart='/usr/bin/php' '/var/www/social/scripts/remove_duplicate_file_urls.php' -y
ExecStart='/usr/bin/php' '/var/www/social/scripts/updateurls.php'
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Updater (Timer)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/social-up.timer'

-------------------------
[Unit]
Description=Daily GNU social Updater and Maintenance

[Timer]
OnCalendar=*-*-* 02:00:00
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Restarter (Service)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/social-re.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/systemctl' stop 'social-d'
ExecStart='/bin/sync'
ExecStart='/usr/bin/systemctl' start 'social-d'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Restarter (Timer)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/social-re.timer'

-------------------------
[Unit]
Description=Daily GNU social Restarter

[Timer]
OnCalendar=*-*-* 02:30:00
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Database Backup Authentication
%%%%%%%%%%%%%%%%%%%%

sudo -u 'mysql' -e '/var/lib/mysqlauth/gnusocial' && sudo chmod '600' '/var/lib/mysqlauth/gnusocial'

-------------------------
[mysqldump]
user=gnusocial
password=x
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% DB Backup (Service)
%%%%%%%%%%%%%%%%%%%%

sudo mkdir -p '/var/lib/mysqltmp' && sudo -e '/etc/systemd/system/social-b.service'

-------------------------
[Service]
Type=oneshot
WorkingDirectory=/var/lib/mysqltmp
ExecStart='/usr/bin/mysqldump' --defaults-extra-file='/var/lib/mysqlauth/gnusocial' --single-transaction 'gnusocial' -r '/var/lib/mysqltmp/gnusocial.sql'
ExecStart='/usr/bin/gzip' -f '/var/lib/mysqltmp/gnusocial.sql'
ExecStart='/usr/bin/bash' -c '"/usr/bin/mv" "/var/lib/mysqltmp/gnusocial.sql.gz" "/mnt/USB/gnusocial-"$$(date +%%Y-%%m-%%d)".sql.gz"'
ExecStart='/usr/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% DB Backup (Timer)
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/social-b.timer'

-------------------------
[Unit]
Description=Daily GNU social database backup

[Timer]
OnCalendar=*-*-* 02:45:00
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Enable and Start Services
%%%%%%%%%%%%%%%%%%%%

sudo systemctl daemon-reload && sudo systemctl enable 'social-d' 'social-up.timer' 'social-re.timer' 'social-b.timer' && sudo systemctl start 'social-d' 'social-up.timer' 'social-re.timer' 'social-b.timer'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Content
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% License
%%%%%%%%%%%%%%%%%%%%

sudo -u 'nginx' wget 'https://licensebuttons.net/l/by-sa/4.0/80x15.png' -O '/var/www/social/cc-by-sa-40.png'

License Text:
Creative Commons Attribution-ShareAlike 4.0 International

License URL:
https://creativecommons.org/licenses/by-sa/4.0

License Image URL:
https://social.realmofespionage.xyz/cc-by-sa-40.png

%%%%%%%%%%%%%%%%%%%%
%%%%% Site Logo
%%%%%%%%%%%%%%%%%%%%

sudo -u 'nginx' wget 'https://i.sli.mg/ThdC4x.png' -O '/var/www/social/roe-social-logo.png'

Site Logo URL:
https://social.realmofespionage.xyz/roe-social-logo.png

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Backup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Server
%%%%%%%%%%%%%%%%%%%%

sudo systemctl stop 'social-d' && cd '/var/www' && zip -r '/home/espionage724/gnusocial.zip' 'social' && sync
sudo mysqldump --defaults-extra-file='/var/lib/mysqlauth/gnusocial' --single-transaction --master-data=2 'gnusocial' -r '/home/espionage724/gnusocial-manual.sql' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Client
%%%%%%%%%%%%%%%%%%%%

scp espionage724@192.168.1.152:'/home/espionage724/gnusocial.zip' espionage724@192.168.1.152:'/home/espionage724/gnusocial-manual.sql' '/home/espionage724/Downloads' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Server (Cleanup)
%%%%%%%%%%%%%%%%%%%%

rm '/home/espionage724/gnusocial-manual.sql' '/home/espionage724/gnusocial.zip' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Restore
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Client
%%%%%%%%%%%%%%%%%%%%

scp '/home/espionage724/Downloads/gnusocial-manual.sql' '/home/espionage724/Downloads/gnusocial.zip' espionage724@192.168.1.152:'/home/espionage724'

%%%%%%%%%%%%%%%%%%%%
%%%%% Server
%%%%%%%%%%%%%%%%%%%%

sync && cd '/home/espionage724' && unzip '/home/espionage724/gnusocial.zip' && sudo mv 'social' '/var/www' && sudo chcon -R -t 'httpd_sys_rw_content_t' '/var/www/social' && sudo chown -R 'nginx':'nginx' '/var/www/social' && sudo chgrp -R 'nginx' '/var/lib/php/session' '/var/lib/php/wsdlcache' && sync
mysql -u 'root' -p 'gnusocial' < '/home/espionage724/gnusocial-manual.sql' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Server (Cleanup)
%%%%%%%%%%%%%%%%%%%%

rm '/home/espionage724/gnusocial-manual.sql' '/home/espionage724/gnusocial.zip' && sync

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################