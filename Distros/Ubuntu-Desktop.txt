%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Xubuntu 16.04

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Repositories
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Wine
%%%%%%%%%%%%%%%%%%%%

sudo add-apt-repository 'ppa:wine/wine-builds' -y

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

sudo apt-get clean && sudo apt-get update && sudo apt-get dist-upgrade -y && sudo apt-get auto-remove -y && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove
%%%%%%%%%%%%%%%%%%%%

sudo apt-get purge parole* gnome-mines* gnome-sudoku* catfish* xfce4-taskmanager* gigolo* && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% General
%%%%%%%%%%%%%%%%%%%%

sudo apt-get install winbind ffmpeg ffmpegthumbnailer mpv winehq-staging unar aria2 p7zip-full mesa-utils gnome-disk-utility keepass2 enigmail android-tools-adb android-tools-fastboot shotwell htop libimage-exiftool-perl gimp epiphany-browser

%%%%%%%%%%%%%%%%%%%%
%%%%% Limited
%%%%%%%%%%%%%%%%%%%%

openjdk-8-jre screenfetch

%%%%%%%%%%%%%%%%%%%%
%%%%% Hammerstorm
%%%%%%%%%%%%%%%%%%%%

handbrake

%%%%%%%%%%%%%%%%%%%%
%%%%% Hailrake
%%%%%%%%%%%%%%%%%%%%

alsa-tools

%%%%%%%%%%%%%%%%%%%%
%%%%% Piety
%%%%%%%%%%%%%%%%%%%%

TODO: Verify if microcode is needed
intel-microcode openssh-server

%%%%%%%%%%%%%%%%%%%%
%%%%% Hatebeat, Oak
%%%%%%%%%%%%%%%%%%%%

sudo apt-get install openssh-server unar aria2 p7zip-full openssh-server

%%%%%%%%%%%%%%%%%%%%
%%%%% Master Password
%%%%%%%%%%%%%%%%%%%%

wget 'https://ssl.masterpasswordapp.com/masterpassword-gui.jar' -O ~/'Documents/masterpassword-gui.jar' && chmod +x ~/'Documents/masterpassword-gui.jar'

%%%%%%%%%%%%%%%%%%%%
%%%%% Google Chrome
%%%%%%%%%%%%%%%%%%%%

TODO: Test

wget 'https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb' -O ~/'Downloads/google-chrome-stable_current_amd64.deb' && sudo dpkg -i ~/'Downloads/google-chrome-stable_current_amd64.deb' && sudo apt-get install -f -y && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Settings
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Keyboard
%%%%%%%%%%%%%%%%%%%%

keepass2
Ctrl + Alt + Z

%%%%%%%%%%%%%%%%%%%%
%%%%% Network
%%%%%%%%%%%%%%%%%%%%

Indoor DNS: 192.168.1.158
Outdoor DNS: https://www.opennicproject.org

%%%%%%%%%%%%%%%%%%%%
%%%%% Printer
%%%%%%%%%%%%%%%%%%%%

AppSocket/HP JetDirect
192.168.1.164

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Other Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Thunar
%%%%%%%%%%%%%%%%%%%%

Smaller icon sizes

TODO: Thunar right-click stuff

%%%%%%%%%%%%%%%%%%%%
%%%%% Mousepad
%%%%%%%%%%%%%%%%%%%%

4 tab width and insert spaces
Show status bar

%%%%%%%%%%%%%%%%%%%%
%%%%% Shotwell (MediaGoblin)
%%%%%%%%%%%%%%%%%%%%

Piwigo
https://media.realmofespionage.xyz/api/piwigo/ws.php

%%%%%%%%%%%%%%%%%%%%
%%%%% Xfce Clock
%%%%%%%%%%%%%%%%%%%%

%c

%%%%%%%%%%%%%%%%%%%%
%%%%% Terminal
%%%%%%%%%%%%%%%%%%%%

0.90 Transparency
10000 Scrollback
Disbale Scroll on output

%%%%%%%%%%%%%%%%%%%%
%%%%% Transmission
%%%%%%%%%%%%%%%%%%%%

http://john.bitsurge.net/public/biglist.p2p.gz

%%%%%%%%%%%%%%%%%%%%
%%%%% Thunderbird
%%%%%%%%%%%%%%%%%%%%

IMAP    mail.cock.li    143 STARTTLS    Normal pass
SMTP    mail.cock.li    587 STARTTLS    Normal pass
Username/Outgoing: espionage724@x.x

%%%%%%%%%%%%%%%%%%%%
%%%%% GPG
%%%%%%%%%%%%%%%%%%%%

TODO (create, backup, restore)

%%%%%%%%%%%%%%%%%%%%
%%%%% mpv
%%%%%%%%%%%%%%%%%%%%

mkdir -p ~/'.config/mpv' && nano ~/'.config/mpv/mpv.conf'

- Change 'vdpau' to 'vaapi' for VA-API
-------------------------
hwdec='vdpau'
vo='opengl-hq'
ao='pulse'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Disable Apport
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/default/apport'

-------------------------
enabled=0
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% UFW (Local)
%%%%%%%%%%%%%%%%%%%%

sudo ufw reset && sudo ufw default deny && sudo ufw enable && sudo ufw logging off && sudo systemctl enable 'ufw'

%%%%%%%%%%%%%%%%%%%%
%%%%% UFW (SSH)
%%%%%%%%%%%%%%%%%%%%

sudo ufw reset && sudo ufw default deny && sudo ufw allow from '192.168.1.0/24' to any port '22' && sudo ufw enable && sudo ufw logging off && sudo systemctl enable 'ufw'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Secure Shell
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Generate SSH Keys
%%%%%%%%%%%%%%%%%%%%

ssh-keygen -o -t 'ed25519'

%%%%%%%%%%%%%%%%%%%%
%%%%% Backup SSH Keys
%%%%%%%%%%%%%%%%%%%%

cd ~ && tar -cvzf ~/'Documents/'$HOSTNAME'-ssh-keys.tar.gz' '.ssh' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Restore SSH Keys
%%%%%%%%%%%%%%%%%%%%

cd ~ && mv ~/'.ssh' ~/'ssh-bak' && tar -xvzf ~/'Documents/'$HOSTNAME'-ssh-keys.tar.gz' '.ssh' && rm ~/'Documents/'$HOSTNAME'-ssh-keys.tar.gz' && sync

%%%%%%%%%%%%%%%%%%%%
%%%%% Copy SSH PubKey
%%%%%%%%%%%%%%%%%%%%

ssh-copy-id 'espionage724@192.168.1.152'
ssh-copy-id 'pi@192.168.1.158'
ssh-copy-id 'espionage724@192.168.1.163'
ssh-copy-id 'beowulfsdr@192.168.1.166'

%%%%%%%%%%%%%%%%%%%%
%%%%% Force PubKey Auth
%%%%%%%%%%%%%%%%%%%%

- Only do after copying ssh keys from main computers

ssh 'espionage724@192.168.1.152'
ssh 'pi@192.168.1.158'
ssh 'espionage724@192.168.1.163'
ssh 'beowulfsdr@192.168.1.166'

sudo -e '/etc/ssh/sshd_config'

-------------------------
PasswordAuthentication no
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Machine Key Labels
%%%%%%%%%%%%%%%%%%%%

ssh 'espionage724@192.168.1.152'
ssh 'pi@192.168.1.158'
ssh 'espionage724@192.168.1.163'
ssh 'beowulfsdr@192.168.1.166'

nano ~/'.ssh/authorized_keys'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Environment Variables
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% ATI/AMD GPU Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/profile.d/r600-debug.sh'

-------------------------
export R600_DEBUG='sbcl,hyperz,llvm,sisched,forcedma'
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Swappiness
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/sysctl.d/swappiness.conf'

-------------------------
vm.swappiness = 10
vm.vfs_cache_pressure = 50
-------------------------

cat '/proc/sys/vm/swappiness'
cat '/proc/sys/vm/vfs_cache_pressure'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Kernel Hardening
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/sysctl.d/harden.conf'

-------------------------
kernel.dmesg_restrict = 1
kernel.kptr_restrict = 1
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Hammerstorm (desktop)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo mkdir -p '/etc/X11/xorg.conf.d' && sudo -e '/etc/X11/xorg.conf.d/graphics.conf'

-------------------------
Section "Device"
    Identifier      "Caribbean Islands"
    Driver          "amdgpu"
    Option          "DRI"           "3"
    Option          "TearFree"      "true"
EndSection

Section "Monitor"
    Identifier      "HDMI-A-0"
    Gamma           0.8
EndSection

Section "Monitor"
    Identifier      "DVI-D-0"
    Gamma           0.8
EndSection

Section "Monitor"
    Identifier      "DVI-I-1"
    Gamma           0.8
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Boot Options
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/default/grub' && sudo update-grub

-------------------------
iommu=pt usbhid.quirks=0x1B1C:0x1B38:0x20000408
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak' && sudo systemctl status 'hd-tweak' -l

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/sbin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Spinecrack (laptop)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/share/X11/xorg.conf.d/99-graphics.conf'

-------------------------
Section "Device"
    Identifier  "Northern Islands"
    Driver      "modesetting"
EndSection

Section "Monitor"
    Identifier  "eDP-1"
    Gamma       0.8
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Boot Options
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/default/grub' && sudo update-grub

-------------------------
usbhid.quirks=0x1B1C:0x1B22:0x20000408
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak'

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/sbin/hdparm' -B '255' '/dev/sda'
ExecStart='/sbin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Hailrake (2-in-1)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/share/X11/xorg.conf.d/99-graphics.conf'

-------------------------
Section "Device"
    Identifier  "Sea Islands"
    Driver      "modesetting"
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Volume Increase
%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/hp-audio.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hp-audio' && sudo systemctl start 'hp-audio'

TODO: Test ''s
-------------------------
[Unit]
Description=Bass enable and gain increase

[Service]
Type=oneshot
ExecStart='/usr/bin/hda-verb' '/dev/snd/hwC1D0' 0x1a 0x782 0x61
ExecStart='/usr/bin/hda-verb' '/dev/snd/hwC1D0' 0x1a 0x773 0x2d

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak'

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/sbin/hdparm' -B '255' '/dev/sda'
ExecStart='/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Piety (mom's laptop)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/share/X11/xorg.conf.d/99-graphics.conf'

-------------------------
Section "Device"
    Identifier  "Haswell"
    Driver      "modesetting"
EndSection

Section "Monitor"
    Identifier  "eDP-0"
    Gamma       0.8
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak'

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/sbin/hdparm' -B '255' '/dev/sda'
ExecStart='/sbin/hdparm' -J '0' --please-destroy-my-drive '/dev/sda'
ExecStart='/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Hatebeat (StepMania)
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Graphics
%%%%%%%%%%%%%%%%%%%%

sudo -e '/usr/share/X11/xorg.conf.d/99-graphics.conf'

-------------------------
Section "Device"
    Identifier  "R600"
    Driver      "modesetting"
EndSection

Section "Monitor"
#    Identifier  "DVI-0"
    Identifier  "DVI-I-1"
    Modeline "640x480_120.00"  52.41  640 680 744 848  480 481 484 515  -HSync +Vsync
EndSection
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% hdparm Tweaks
%%%%%%%%%%%%%%%%%%%%

sudo -e '/lib/systemd/system/hd-tweak.service' && sudo systemctl daemon-reload && sudo systemctl enable 'hd-tweak' && sudo systemctl start 'hd-tweak'

-------------------------
[Unit]
Description=hdparm Tweaks

[Service]
Type=oneshot
ExecStart='/sbin/hdparm' -A '1' '/dev/sda'
ExecStart='/sbin/hdparm' -B '255' '/dev/sda'
ExecStart='/sbin/hdparm' -M '0' '/dev/sda'
ExecStart='/sbin/hdparm' -S '0' '/dev/sda'
ExecStart='/sbin/hdparm' -W '1' '/dev/sda'

[Install]
WantedBy=multi-user.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% PulseAudio
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nano ~/'.config/pulse/daemon.conf'

-------------------------
resample-method = soxr-mq
flat-volumes = no
deferred-volume-safety-margin-usec = 1
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Automatic Updates
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/ubuntu-up.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/apt-get' clean
ExecStart='/usr/bin/apt-get' update
ExecStart='/usr/bin/apt-get' dist-upgrade -y
ExecStart='/usr/bin/apt-get' auto-remove -y
ExecStart='/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/ubuntu-up.timer' && sudo systemctl daemon-reload && sudo systemctl enable 'ubuntu-up.timer' && sudo systemctl start 'ubuntu-up' 'ubuntu-up.timer' && sudo systemctl status 'ubuntu-up' -l

-------------------------
[Unit]
Description=Daily sources refresh and dist-upgrade

[Timer]
OnCalendar=x
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Times
%%%%%%%%%%%%%%%%%%%%

- Hammerstorm:  05:00:00
- Spinecrack:   05:10:00
- Hailrake:     05:20:00
- Piety:        05:30:00
- Hatebeat:     05:40:00
- Oak:          06:00:00

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Steven Black's Unified Hosts File
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hosts-up.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/bin/wget' 'https://github.com/StevenBlack/hosts/archive/master.zip' -O '/tmp/master.zip'
ExecStart='/usr/bin/unzip' '/tmp/master.zip' -d '/tmp'
ExecStart='/usr/bin/python' '/tmp/hosts-master/updateHostsFile.py' --auto --replace
ExecStart='/bin/rm' -R '/tmp/master.zip' '/tmp/hosts-master'
ExecStart='/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/hosts-up.timer' && sudo systemctl daemon-reload && sudo systemctl enable 'hosts-up.timer' && sudo systemctl start 'hosts-up' 'hosts-up.timer' && sudo systemctl status 'hosts-up' -l

-------------------------
[Unit]
Description=Weekly hosts source refresh

[Timer]
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% ext4 Defragmentation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Service
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/fs-m.service'

-------------------------
[Service]
Type=oneshot
ExecStart='/usr/sbin/e4defrag' '/dev/mapper/xubuntu--vg-root'
ExecStart='/bin/sync'
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Timer
%%%%%%%%%%%%%%%%%%%%

sudo -e '/etc/systemd/system/fs-m.timer' && sudo systemctl daemon-reload && sudo systemctl enable 'fs-m.timer' && sudo systemctl start 'fs-m' 'fs-m.timer' && sudo systemctl status 'fs-m' -l

-------------------------
[Unit]
Description=Weekly ext4 Defrag

[Timer]
OnCalendar=weekly
Persistent=true

[Install]
WantedBy=timers.target
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Firefox Setup
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% about:config
%%%%%%%%%%%%%%%%%%%%

cd ~/'.mozilla/firefox/'*'.default' && nano 'user.js' && cd ~

-------------------------
user_pref("media.mp3.enabled", true);
user_pref("layout.frame_rate.precise", true);
user_pref("mousewheel.min_line_scroll_amount", 40);
user_pref("javascript.options.mem.high_water_mark", 32);
user_pref("javascript.options.mem.max", 51200);
user_pref("dom.storage.enabled", true);
user_pref("dom.event.clipboardevents.enabled", true);
user_pref("media.fragmented-mp4.exposed", true);
user_pref("media.fragmented-mp4.ffmpeg.enabled", true);
user_pref("media.mediasource.ignore_codecs", true);
user_pref("layers.acceleration.force-enabled", true);
user_pref("layers.offmainthreadcomposition.enabled", true);
user_pref("extensions.jid1-BoFifL9Vbdl2zQ@jetpack.showReleaseNotes", false);
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Extenstions (7)
%%%%%%%%%%%%%%%%%%%%

xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/privacy-settings/versions' && xdg-open 'https://www.eff.org/privacybadger' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/versions' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/gnotifier/versions' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/versions' && xdg-open 'https://www.eff.org/https-everywhere' && xdg-open 'https://addons.mozilla.org/en-US/firefox/addon/disable-hello-pocket-reader/versions'

%%%%%%%%%%%%%%%%%%%%
%%%%% Extensions Configuration
%%%%%%%%%%%%%%%%%%%%

- uBlock filters (disable EasyList + EasyPrivacy, enable Easy+Fanboy Ultimate + both Adblock killers)
- Full privacy with Privacy Settings button

%%%%%%%%%%%%%%%%%%%%
%%%%% Search Engines
%%%%%%%%%%%%%%%%%%%%

https://searx.me/about#
https://startpage.com/eng/download-startpage-plugin.html?hmb=1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Noteable Folders and Commands
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Shortcuts
%%%%%%%%%%%%%%%%%%%%

/usr/share/applications
~/.local/share/applications

%%%%%%%%%%%%%%%%%%%%
%%%%% Icons
%%%%%%%%%%%%%%%%%%%%

/usr/share/icons/hicolor
~/.local/share/icons/hicolor

%%%%%%%%%%%%%%%%%%%%
%%%%% sources.list
%%%%%%%%%%%%%%%%%%%%

/etc/apt/sources.list

%%%%%%%%%%%%%%%%%%%%
%%%%% Check CPU Frequency
%%%%%%%%%%%%%%%%%%%%

grep 'MHz' '/proc/cpuinfo'
watch -n 0.1 grep \'cpu MHz\' '/proc/cpuinfo'

%%%%%%%%%%%%%%%%%%%%
%%%%% Encryption Info
%%%%%%%%%%%%%%%%%%%%

sudo cryptsetup luksDump '/dev/sda3'
sudo cryptsetup status '/dev/mapper/sda3_crypt'

%%%%%%%%%%%%%%%%%%%%
%%%%% Hard Drive Wipe
%%%%%%%%%%%%%%%%%%%%

- Make sure to change sda if not wanting to do sda

sudo hdparm -I '/dev/sda' | grep 'not'
sudo hdparm --user-master u --security-set-pass 'x' '/dev/sda'
sudo hdparm --user-master u --security-erase-enhanced 'x' '/dev/sda'

%%%%%%%%%%%%%%%%%%%%
%%%%% Get GCC Compile Flags
%%%%%%%%%%%%%%%%%%%%

gcc -v -E -x c -march=native -mtune=native - < /dev/null 2>&1 | grep cc1 | perl -pe 's/ -mno-\S+//g; s/^.* - //g;'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Privacy
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Remove metadata
%%%%%%%%%%%%%%%%%%%%

exiftool -all= *.* -overwrite_original

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################